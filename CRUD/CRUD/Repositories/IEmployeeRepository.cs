﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD.Repositories
{
    public interface IEmployeeRepository
    {
        void Create (int id, string name, string lastName);
        Employee Get(int id);
        List<Employee> GetAll();
        void Update(int id, string name, string lastName);
        void Delete(int id);
    }
}
