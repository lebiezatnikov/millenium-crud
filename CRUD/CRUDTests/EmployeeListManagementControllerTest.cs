﻿using CRUD;
using CRUD.Controllers;
using CRUD.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRUDTests
{
    public class EmployeeServiceTest
    {
        private EmployeeService employeeService;


        [SetUp]
        public void Setup()
        {
            
        }

        [TestCase("25")]
        public void GetByIdTest(int id)
        {
            Assert.IsTrue(employeeService.GetById(id) != null);
        }
    }

}
