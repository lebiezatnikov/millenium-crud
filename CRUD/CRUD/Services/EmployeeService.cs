﻿using CRUD.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD.Services
{
    public class EmployeeService: IEmpolyeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public Employee GetById(int id)
        {            
            return _employeeRepository.Get(id);
        }

        public List<Employee> GetAll()
        {
            return _employeeRepository.GetAll();
        }

        public void Delete(int id)
        {
            _employeeRepository.Delete(id);
        }

        public void Update(int id, string name, string lastName)
        {
            _employeeRepository.Update(id, name, lastName);
        }

        public void Create(int id, string name, string lastName)
        {
            _employeeRepository.Create(id, name, lastName);
        }
    }
}
