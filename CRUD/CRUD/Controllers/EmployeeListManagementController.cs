﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUD.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CRUD.Controllers
{
    [ApiController]
    [Route("employee")]
    public class EmployeeListManagementController : ControllerBase
    {
        private readonly IEmpolyeeService _employeeService;

        public EmployeeListManagementController(IEmpolyeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        [Route("get/{id}")]
        public IActionResult Get(int id)
        {
            var employee = _employeeService.GetById(id);
            return Ok(employee);
        }

        [HttpGet]
        [Route("get-all")]
        public IActionResult GetAll()
        {
            var employees = _employeeService.GetAll();
            return Ok(employees);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _employeeService.Delete(id);
            return Ok();
        }

        [HttpPut]
        [Route("update/{id}/{name}/{lastName}")]
        public IActionResult Update(int id, string name, string lastName)
        {
            _employeeService.Update(id, name, lastName);
            return Ok();
        }

        [HttpPost]
        [Route("create/{id}/{name}/{lastName}")]
        public IActionResult Create(int id, string name, string lastName)
        {
            _employeeService.Create(id, name, lastName);
            return Ok();
        }
    }
}