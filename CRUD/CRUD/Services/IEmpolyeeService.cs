﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD.Services
{
    public interface IEmpolyeeService
    {
        Employee GetById(int id);
        List<Employee> GetAll();
        void Delete(int id);
        void Update(int id, string name, string lastName);
        void Create(int id, string name, string lastName);
    }
}
