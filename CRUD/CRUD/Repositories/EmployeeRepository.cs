﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace CRUD.Repositories
{
    public class EmployeeRepository: IEmployeeRepository
    {        

        public void Create(int id, string name, string lastName)
        {
            var employeeList = new List<Employee>();
            var e = new Employee { Id = id, Name = name, LastName = lastName };
            employeeList.Add(e);
        }

        public void Delete(int id)
        {
            var employeeList = new List<Employee>();
            employeeList.Add(new Employee { Id = 1, Name = "TestName", LastName = "TestLastName" });
            employeeList.Add(new Employee { Id = 2, Name = "TestName2", LastName = "TestLastName2" });
            employeeList.Add(new Employee { Id = 3, Name = "TestName3", LastName = "TestLastName3" });

            var e = employeeList.FirstOrDefault(x => x.Id == id);
            employeeList.Remove(e);
        }

        public Employee Get(int id)
        {
            var employeeList = new List<Employee>();
            employeeList.Add(new Employee { Id = 1, Name = "TestName", LastName = "TestLastName" });
            employeeList.Add(new Employee { Id = 2, Name = "TestName2", LastName = "TestLastName2" });
            employeeList.Add(new Employee { Id = 3, Name = "TestName3", LastName = "TestLastName3" });

            return employeeList.Where(e => e.Id == id).SingleOrDefault();
        }

        public List<Employee> GetAll()
        {
            var employeeList = new List<Employee>();
            employeeList.Add(new Employee { Id = 1, Name = "TestName", LastName = "TestLastName" });
            employeeList.Add(new Employee { Id = 2, Name = "TestName2", LastName = "TestLastName2" });
            employeeList.Add(new Employee { Id = 3, Name = "TestName3", LastName = "TestLastName3" });

            return employeeList;
        }

        public void Update(int id, string name, string lastName)
        {
            var employeeList = new List<Employee>();
            employeeList.Add(new Employee { Id = 1, Name = "TestName", LastName = "TestLastName" });
            employeeList.Add(new Employee { Id = 2, Name = "TestName2", LastName = "TestLastName2" });
            employeeList.Add(new Employee { Id = 3, Name = "TestName3", LastName = "TestLastName3" });

            var obj = employeeList.FirstOrDefault(x => x.Id == id);
            if (obj != null)
            {
                obj.Name = name;
                obj.LastName = lastName;
            }
        }
    }
}
